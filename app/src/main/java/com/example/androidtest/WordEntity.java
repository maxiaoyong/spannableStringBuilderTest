package com.example.androidtest;

/**
 * 汉字或单词实体类，记录汉字或单词内容以及在文本中开始和结束index
 *
 * @author maxiaoyong
 */
public class WordEntity {

    private String word;
    private int startIndex;
    private int endIndex;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }
}
