package com.example.androidtest;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author maxiaoyong
 */
public class Util {

    private static ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

    /**
     * 英文单词正则表达式
     * <p>
     * 注:在使用正则表达式时，利用其预编译功能，可以有效加快正则匹配速度,不要在方法体内定义
     */
    private static final Pattern PATTERN_ENG = Pattern.compile("[a-zA-Z-']+");


    /**
     * 汉字正则表达式
     * <p>
     * 匹配每一个字用Pattern pattern = Pattern.compile("[\\u4e00-\\u9fa5]");
     * 匹配整句用Pattern pattern = Pattern.compile("^((?!(\\*|//)).)+[\\u4e00-\\u9fa5]");
     * <p>
     * 注:在使用正则表达式时，利用其预编译功能，可以有效加快正则匹配速度,不要在方法体内定义
     */
    private static final Pattern PATTERN_CH = Pattern.compile("[\\u4e00-\\u9fa5]");

    /**
     * 通过正则表达式获取String中所有英文单词
     */
    public static List<String> splitWord(@NonNull String text) {

        if (TextUtils.isEmpty(text)) {
            return new ArrayList<>();
        }

        List<String> words = new ArrayList<>();
        Matcher matcher = PATTERN_ENG.matcher(text);
        while (matcher.find()) {
            words.add(matcher.group(0));
        }
        return words;
    }


    /**
     * 通过正则表达式获取String中所有汉字(非词组)
     */
    public static List<String> splitChinese(@NonNull String text) {

        if (TextUtils.isEmpty(text)) {
            return new ArrayList<>();
        }

        List<String> words = new ArrayList<>();
        Matcher matcher = PATTERN_CH.matcher(text);
        while (matcher.find()) {
            words.add(matcher.group(0));
        }
        return words;
    }

    private static Toast mToast;

    /**
     * Toast单例,适配了Android 9 由于源码改动导致连续调用后，Toast不显示问题。
     * 但是在Android 10 上仍然存在连续调用后不显示的问题。
     *
     * @param context
     * @param text
     * @param duration
     */
    public static void showToast(Context context, String text, int duration) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.P) {
            Toast.makeText(context, text, duration).show();
        } else {
            if (mToast == null) {
                mToast = Toast.makeText(context, text, duration);
            } else {
                mToast.setText(text);
                mToast.setDuration(duration);
            }
            mToast.show();
        }
    }
}